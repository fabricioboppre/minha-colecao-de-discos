import React from 'react'
import Link from 'next/link'

export default function Navigation(props) {
	let navButtonLabel = (props.mediumBodyNavSituation === 'open' ? "esconder opções" : "mostrar opções")
	let mediumBodyNewNavSituation = (props.mediumBodyNavSituation === 'open' ? "closed" : "open")
	return <nav id="navigation">
				<ul className={props.mediumBodyNavSituation === 'open' ? "open" : "closed-for-medium-body"}>
					<li>
						<NavLayout layoutFormat={props.layoutFormat} 
								   handleClickLayout={props.handleClickLayout} />
					</li>
					<li>
						<NavSortOrder sort={props.sort}
								  	  order={props.order} />
					</li>
				</ul>
	    	   <button className='control-button'
			   		   value={mediumBodyNewNavSituation} 
	    			   onClick={e => props.handleClickNavButton(e, "value")}>
					   {navButtonLabel}
			   </button>
		   </nav>
}

function NavLayout(props) {
	return 	<React.Fragment>
				<span className="nav_title">Visualizar como:</span>
				<ul>
				    <NavLayoutButton currentValue={props.layoutFormat}
				    		 		 thisValue="mosaic"
				    		 		 label="Mosaico"
				    		 		 handleClick={props.handleClickLayout} />
				    <NavLayoutButton currentValue={props.layoutFormat}
				    		 		 thisValue="table"
				    		 		 label="Tabela"
				    		 		 handleClick={props.handleClickLayout} />
				</ul>
			</React.Fragment>
}

function NavLayoutButton(props) {
    return <li className={props.currentValue === props.thisValue ? "active" : ""} >
	    	   <button value={props.thisValue} 
	    			   onClick={e => props.handleClick(e, "value")}>
					   {props.label}
			   </button>
			   {props.children}
		   </li>
}

function NavSortOrder(props) {
	return  <React.Fragment>
				<span className="nav_title">Ordenar por:</span>
				<ul>
				    <NavSortLink sortCurrentValue={props.sort}
				    		 	 sortThisValue="artist"
				    		 	 label="Artista" >
						    <NavOrderLinks sortThisValue="artist"
										   orderCurrentValue={props.order} />
					</NavSortLink>

				    <NavSortLink sortCurrentValue={props.sort}
				    		 	 sortThisValue="title"
				    		 	 label="Título" >
						    <NavOrderLinks sortThisValue="title"
										   orderCurrentValue={props.order} />
					</NavSortLink>

				    <NavSortLink sortCurrentValue={props.sort}
				    		 	 sortThisValue="year"
				    		 	 label="Ano de lançamento" >
						    <NavOrderLinks sortThisValue="year"
										   orderCurrentValue={props.order} />
					</NavSortLink>
				</ul>
			</React.Fragment>
}

function NavSortLink(props) {
    return <li className={props.sortCurrentValue === props.sortThisValue ? "active" : ""}>
		        <Link href={{pathname: '/',
							 query: { sort: props.sortThisValue,
						    		  order: 'asc',
								      page: '1' } }}>
		          <a><button>{props.label}</button></a>
		        </Link>
				{props.children}
			</li>
}

function NavOrderLinks(props) {
    return <ul className="parameters">
				<li className={props.orderCurrentValue === 'asc' ? "active" : ""}>
			        <Link href={{pathname: '/', 
								 query: { sort: props.sortThisValue, 
								 		  order: 'asc',
									      page: '1' } }}>
			          <a><button>▲</button></a>
			        </Link>
				</li>
				<li className={props.orderCurrentValue === 'desc' ? "active" : ""}>
			        <Link href={{pathname: '/', 
								 query: { sort: props.sortThisValue, 
								 		  order: 'desc',
									      page: '1' } }}>
			          <a><button>▼</button></a>
			        </Link>
				</li>
			</ul>
}
