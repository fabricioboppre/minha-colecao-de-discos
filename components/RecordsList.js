import Image from "next/image";
import Link from "next/link";

export default function RecordsList(props) {
  const RecordsList = props.records.map((recordMapped) => (
    <Record recordMapped={recordMapped} key={recordMapped.instance_id} />
  ));
  return (
    <div id="records">
      <ul className={props.layoutFormat}>{RecordsList}</ul>
      <Pagination
        pagination={props.pagination}
        sort={props.sort}
        order={props.order}
        page={props.page}
        recordsLenght={props.records.length}
      />
    </div>
  );
}

function Record(props) {
  return (
    <li>
      <a
        href={
          "https://www.discogs.com/release/" +
          props.recordMapped.basic_information.id
        }
      >
        <div className="image">
          <img
            alt={props.recordMapped.basic_information.title}
            src={props.recordMapped.basic_information.cover_image}
          />
        </div>
        <div className="text">
          <ArtistsList artists={props.recordMapped.basic_information.artists} />
          <div className="title_year">
            <span className="title">
              {props.recordMapped.basic_information.title}
            </span>{" "}
            <span>
              (
              {props.recordMapped.basic_information.year === 0
                ? "ano de lançamento não-informado"
                : props.recordMapped.basic_information.year}
              )
            </span>
          </div>
          <GenresList genres={props.recordMapped.basic_information.genres} />
          <FormatsList formats={props.recordMapped.basic_information.formats} />
        </div>
      </a>
    </li>
  );
}

function ArtistsList(props) {
  const ArtistsList = props.artists
    .map((artistMapped, key) => (
      <span key={artistMapped.id}>{artistMapped.name}</span>
    ))
    .reduce((prev, curr) => [prev, ", ", curr]);
  return <div className="artists">{ArtistsList}</div>;
}

function GenresList(props) {
  const GenresList = props.genres
    .map((genreMapped, key) => <span key={key}>{genreMapped}</span>)
    .reduce((prev, curr) => [prev, " / ", curr]);
  return <div className="genres">{GenresList}</div>;
}

function FormatsList(props) {
  const FormatsList = props.formats
    .map((formatMapped, key) => <span key={key}>{formatMapped.name}</span>)
    .reduce((prev, curr) => [prev, " / ", curr]);
  return <div className="formats">[{FormatsList}]</div>;
}

function Pagination(props) {
  return (
    <div id="pagination">
      <div
        id="pag_prev"
        className={
          typeof props.pagination.urls.prev === "undefined" ? "" : "active"
        }
      >
        <Link
          href={{
            pathname: "/",
            query: {
              sort: props.sort,
              order: props.order,
              page: parseInt(props.page) - 1,
            },
          }}
        >
          <a>
            <button>«</button>
          </a>
        </Link>
      </div>
      <div id="pag_info">
        <p>
          Exibindo {props.recordsLenght} de um total de {props.pagination.items}{" "}
          discos
        </p>
        <p>
          (Página {props.pagination.page} de {props.pagination.pages})
        </p>
      </div>
      <div
        id="pag_next"
        className={
          typeof props.pagination.urls.next === "undefined" ? "" : "active"
        }
      >
        <Link
          href={{
            pathname: "/",
            query: {
              sort: props.sort,
              order: props.order,
              page: parseInt(props.page) + 1,
            },
          }}
        >
          <a>
            <button>»</button>
          </a>
        </Link>
      </div>
    </div>
  );
}
