import '../styles/globals.css'
import '../styles/reset-min.css'
import App from 'next/app'
import Router from 'next/router'
import NProgress from 'nprogress'
import Script from 'next/script'

// PAGE LOADING INDICATOR:
// - https://github.com/vercel/next.js/tree/master/examples/with-loading
Router.events.on('routeChangeStart', (url) => NProgress.start())
Router.events.on('routeChangeComplete', () => NProgress.done())
Router.events.on('routeChangeError', () => NProgress.done())

export default class MyRecordCollection extends App {

  render () {

    const { Component, pageProps } = this.props
    return (
			<>
				<Component {...pageProps} />
				<Script src="/js/modernizr-custom.js"></Script>
			</>
    )
  }

}