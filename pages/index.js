import React from 'react'
import Head from 'next/head'
import RecordsList from '../components/RecordsList.js'
import Navigation from '../components/Navigation.js'

export default class Index extends React.Component {

	// Initial State:
  state = {layoutFormat: 'mosaic',
           mediumBodyNavSituation: 'open',
					 sort: 'artist',
					 order: 'asc',
					 page: 1};

  // Invoke right before calling the render method, both on the initial mount and on subsequent updates:
  // - If there is a query, update the state with the parameters of this query;
  // - Queries arise when sorting or paging options are clicked;
  // - https://reactjs.org/docs/react-component.html#static-getderivedstatefromprops
  static getDerivedStateFromProps(props, prevState) {
      // Run only when there are parameters in the URL (query):
      if (Object.keys(props.query).length > 0) {
        // Ensures the existence of values for all items in the state, even if the URL is manipulated and the query is incomplete (in this case, the missing items will assume the default values of the initial state):
        let querySort, queryOrder, queryPage
        if (props.query.sort) { querySort = props.query.sort } else { querySort = prevState.sort}
        if (props.query.order) { queryOrder = props.query.order } else { queryOrder = prevState.order}
        if (props.query.page) { queryPage = props.query.page } else { queryPage = prevState.page}
        // Updates the state, which causes a new rendering:
        return { sort: querySort,
                 order: queryOrder,
                 page: queryPage };
      }
      return null;
  }

	// Methods: 
	// - Arrow functions make .bind method calls in the constructor unnecessary;
	// - (Because they lexically bind their context so this actually refers to the originating context; that’s called Lexical Scoping.)
	// - https://www.taniarascia.com/es6-syntax-and-feature-overview/#arrow-functions
	// - https://medium.com/@nikolalsvk/loosing-bind-this-in-react-8637ebf372cf

  // Method that handles layout change:
	handleClickLayout = (e) => { this.setState({layoutFormat: e.target.value}) }

  // Method that controls the display of navigation options (when the screen is medium width or less):
	handleClickNavButton = (e) => { this.setState({mediumBodyNavSituation: e.target.value}) }

  render() {
    const records = this.props.data.releases
    const pagination = this.props.data.pagination
    let content
    if (this.props.data.message) {
      content = <p className="error">Ops! Estamos com o seguinte problema na conexão com o Discogs: {this.props.data.message}</p>
    } else {
      content = (
					<React.Fragment>
            <Navigation sort={this.state.sort} 
    					          order={this.state.order}
                        mediumBodyNavSituation={this.state.mediumBodyNavSituation}
        								layoutFormat={this.state.layoutFormat}
                        handleClickLayout={this.handleClickLayout}
                        handleClickNavButton={this.handleClickNavButton} />
            <RecordsList records={records}
                         pagination={pagination}
              					 sort={this.state.sort} 
    					           order={this.state.order}
                         page={this.state.page}
                         layoutFormat={this.state.layoutFormat} />
          </React.Fragment>
      )         
    }
    return (
      <React.Fragment>
  
        <Head>
          <title>Minha coleção de discos</title>
        </Head>
  
        <main id="minha-colecao-de-discos">        

          <h1>Minha coleção de discos</h1>
          
          <div className="intro">
            <p>Um pequeno site experimental de <a href="http://fabricioboppre.net">Fabricio C. Boppré</a>. Construído com <a href="http://nextjs.org">Next.js</a> e a <a href="https://www.discogs.com/developers">API</a> do Discogs.</p> 
          </div>
                  
          {content}

    			<div id="fallback">
    				<p className="error">Este pequeno experimento utiliza <a href="https://caniuse.com/#feat=flexbox">CSS Flexible Box Layout Module</a>, e parece que o seu browser não suporta este recurso. Está na hora de atualizá-lo!</p>
    			</div>
    
        </main>


      </React.Fragment>
    )
  }

}
  
// This gets called on every request (Server-side Rendering):
export async function getServerSideProps({ query }) {
  // Authentication data in the discogs API:
  // - Stored in the .env.local file in the development environment;
  // - In "Environment Variables" settings on Vercel, in the production environment.  
	const userToken = process.env.DISCOGS_USER_TOKEN
	const userID = process.env.DISCOGS_USER_ID
	const folderID = process.env.DISCOGS_FOLDER_ID
  // Query values (if they do not come via URL, they assume initial values):
  const sort = (query.sort) ? query.sort : 'artist'
  const order = (query.order) ? query.order : 'asc'
  const page = (query.page) ? query.page : '1'
  // Fetch the data from the discogs API:
	const collectionItemsByFolderURL = 'https://api.discogs.com/users/' + userID + '/collection/folders/' + folderID + '/releases?sort=' + sort + '&sort_order=' + order + '&page=' + page + '&per_page=42'
	const init = { headers: {'User-Agent': 'MyRecordsCollection/1.0.0 +http://www.fabricioboppre.net',
							             'Authorization': 'Discogs token=' + userToken } }
  const res = await fetch(collectionItemsByFolderURL, init)
  const data = await res.json()
  // Pass data to the page via props:
  return { props: { data, query } }
}