# (Em português)

## Introdução

Este pequeno site nasceu enquanto eu dava meus primeiros passos na biblioteca JavaScript [React](https://reactjs.org) e na [API](https://www.discogs.com/developers) do site Discogs. Enquanto estudava e programava, conheci o [Next.js](https://nextjs.org), e logo me interessei pelas vantagens que este framework oferece. O [Discogs](http://www.discogs.com) é um banco de dados de discos mantido coletivamente, com o qual gosto de contribuir devido à minha paixão por música. Estou cadastrando lá, pouco a pouco, minha coleção de discos (portanto o que o site exibe, neste momento, não é ainda minha coleção completa).

Você pode visitá-lo clicando [aqui](https://meus-discos.fabricioboppre.net/). O site utiliza o modo de pré-renderização [Server-side Rendering](https://nextjs.org/docs/basic-features/pages#pre-rendering) e meu deployment particular tem como destino final a plataforma [Vercel](https://vercel.com/home).

## Configuração

Se você não possui nenhuma experiência com o Next.js, comece por [aqui](https://nextjs.org/docs/getting-started).

Após clonar o projeto (`git clone git@bitbucket.org:fabricioboppre/minha-colecao-de-discos.git`), não esqueça de instalar suas dependências (`npm install`).

Para visualizar sua própria coleção do Discogs em sua versão deste site, é necessário configurar três variáveis de ambiente:

* DISCOGS_USER_TOKEN: Seu *user token* no Discogs;
* DISCOGS_USER_ID: O seu *username* no Discogs;
* DISCOGS_FOLDER_ID: O ID da *folder* onde estão seus discos (use 0 caso não utilize *folders*).

Informações sobre as variáveis acima você encontra na [documentação da API](https://www.discogs.com/developers/) do Discogs. Informações sobre variáveis de ambiente você encontra [aqui](https://nextjs.org/docs/basic-features/environment-variables).

Para o deployment, recomendo a [Vercel](https://nextjs.org/docs/deployment).

## Tradução

Os comentários ao longo do código-fonte estão em inglês. Se algum brasileiro interessado em utilizá-lo encontrar alguma dificuldade, é só entrar em [contato](mailto:fabricio.boppre@gmail.com).

## Licença

O código-fonte deste site está compartilhado sob a licença MIT. Para mais informações, leia o arquivo [LICENSE](LICENSE).

# (In English)

## Introduction

This small website was born while I was taking my first steps in the JavaScript library [React](https://reactjs.org) and the [Discogs API](https://www.discogs.com/developers). While studying and coding, I got to know [Next.js](https://nextjs.org) and soon became interested in the advantages this framework offers. [Discogs](http://www.discogs.com) is a collectively maintained database of records that I like to contribute to because of my love of music and records. I'm slowly adding my records collection there (so what the website displays at this moment is not yet my complete collection).

You can visit the website [here](https://meus-discos.fabricioboppre.net/). It uses [Server-side Rendering](https://nextjs.org/docs/basic-features/pages#pre-rendering) and my deployment has [Vercel](https://vercel.com/home) as its final destination.

## Configuration

If you have no experience with Next.js, start [here](https://nextjs.org/docs/getting-started).

After cloning the project (`git clone git@bitbucket.org: fabricioboppre / minha-colecao-de-discos.git`), don't forget to install its dependencies (`npm install`).

To see your own Discogs collection in your version of this website, you have to set three environment variables:

* DISCOGS_USER_TOKEN: Your Discogs user token;
* DISCOGS_USER_ID: Your Discogs username;
* DISCOGS_FOLDER_ID: The ID of the folder to request (use 0 if you don't have folders).

More information about these variables you find at the [Discogs API documentation](https://www.discogs.com/developers/). Information about environment variables you can find [here](https://nextjs.org/docs/basic-features/environment-variables).

For deployment, I suggest [Vercel](https://nextjs.org/docs/deployment).

## Translation

The frontend is in Portuguese. If you are a non-Portuguese speaker who wants to change something you don't understand, I'll be glad to [help](mailto:fabricio.boppre@gmail.com).

## License

The source code of this website is shared under the MIT license. For more information, see [LICENSE](LICENSE).